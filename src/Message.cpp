#include "Message.h"
#include <cstring>

namespace chat {
Message::Message(MessageType messageType, std::string body)
    : mMessageType{messageType}, mBody{std::move(body)} {}

Message::MessageType Message::type() const { return mMessageType; }

const std::string &Message::body() const { return mBody; }

std::size_t
Message::decodeHeader(const std::array<std::byte, MessageHeaderSize> &bytes) {
  std::size_t messageSize;
  std::memcpy(&messageSize, bytes.data(), MessageHeaderSize);
  return messageSize;
}

std::array<std::byte, Message::MessageHeaderSize>
Message::encodeHeader(std::size_t value) {
  std::array<std::byte, MessageHeaderSize> result;
  std::memcpy(result.data(), &value, MessageHeaderSize);
  return result;
}

std::vector<std::byte> Message::encode() const {
  auto messageData = std::vector<std::byte>(mBody.size() + sizeof(MessageType));
  std::memcpy(messageData.data(), &mMessageType, sizeof(MessageType));
  std::memcpy(messageData.data() + sizeof(MessageType), mBody.data(),
              mBody.size());
  return messageData;
}

std::shared_ptr<Message>
Message::decode(const std::vector<std::byte> &messageData) {
  MessageType type;
  std::string body;
  body.resize(messageData.size() - sizeof(MessageType));
  std::memcpy(&type, messageData.data(), sizeof(MessageType));
  std::memcpy(body.data(), messageData.data() + sizeof(MessageType),
              body.size());
  return std::make_shared<Message>(type, body);
}

} // namespace chat
