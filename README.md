# Simple Chat Server

## Compilation
Run `docker build . -t chat`.
Additional CMAKE arguments can be provided with `--build-arg CMAKE_ARGS=`.
For example `--build-arg CMAKE_ARGS="-DCMAKE_BUILD_TYPE=DEBUG -DADDRESS_SANITIZER=ON"`.

## Running the server
The server can be started with `docker run --rm --network=host chat PORT THREAD_POOL_SIZE`.
For example `docker run --rm chat 9999`.

## Running the client
A client can be started with `docker run --rm -ti --network=host --entrypoint /bin/chat/client chat HOST PORT`.
For example `docker run --rm --entrypoint /bin/chat/client chat localhost 9999`.

### Using the client
The client waits for input on standard in and uses the following syntax `CMD: [CONTENT]`.
The following commands are supported:
* LOGIN: CONTENT
    * Login the client with the username specified by CONTENT
* MESSAGE: CONTENT
    * Send a message to all other connected clients with the content specified by CONTENT.
* STOP:
    * Stops the client. Alternatively CTRL+C can be used.
---
**NOTE**
It is required that a client first sends a message, before it can send a text message.

---


