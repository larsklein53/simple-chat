#include "Session.h"
#include <boost/asio.hpp>
#include <boost/asio/co_spawn.hpp>
#include <boost/asio/use_awaitable.hpp>
#include <charconv>
#include <iostream>

namespace chat {

Session::Session(SSLSocket socket)
    : mSocket{std::move(socket)}, mTimer{mSocket.get_executor()},
      mReadStrand{mSocket.get_executor()}, mWriteStrand{
                                               mSocket.get_executor()} {
  mTimer.expires_at(std::chrono::steady_clock::time_point::max());
}

Session::Session(boost::asio::ip::tcp::socket socket,
                 boost::asio::ssl::context &sslContext)
    : Session(SSLSocket{std::move(socket), sslContext}) {}

Session::Session(boost::asio::io_context &ioContext,
                 boost::asio::ssl::context &sslContext)
    : Session(SSLSocket{ioContext, sslContext}) {}

Session::SSLSocket &Session::getSocket() { return mSocket; }

boost::asio::awaitable<void>
Session::start(MessageHandler &&messageHandler, ErrorHandler &&errorHandler,
               boost::asio::ssl::stream_base::handshake_type handshakeType) {
  try {
    co_await mSocket.async_handshake(handshakeType, boost::asio::use_awaitable);
  } catch (const std::exception &exception) {
    std::cerr << "Exception: " << exception.what() << "\n";
    close();
    co_return;
  }
  co_spawn(
      mReadStrand,
      [self = shared_from_this(), messageHandler,
       errorHandler]() -> boost::asio::awaitable<void> {
        return self->reader(messageHandler, errorHandler);
      },
      boost::asio::detached);

  co_spawn(
      mWriteStrand,
      [self = shared_from_this(),
       errorHandler]() -> boost::asio::awaitable<void> {
        return self->writer(errorHandler);
      },
      boost::asio::detached);
}

boost::asio::awaitable<void>
Session::reader(const MessageHandler &messageHandler,
                const ErrorHandler &errorHandler) {
  try {
    while (true) {
      auto headerData = std::array<std::byte, Message::MessageHeaderSize>{};
      const auto bytes = co_await boost::asio::async_read(
          mSocket, boost::asio::buffer(headerData), boost::asio::use_awaitable);
      if (bytes != Message::MessageHeaderSize) {
        throw std::runtime_error{"Invalid message header received"};
      }
      const auto messageSize = Message::decodeHeader(headerData);
      const auto message = co_await readBody(messageSize);
      messageHandler(message);
    }
  } catch (const std::exception &exception) {
    std::cerr << exception.what() << "\n";
    close();
    errorHandler();
    co_return;
  }
}

boost::asio::awaitable<std::shared_ptr<Message>>
Session::readBody(std::size_t messageSize) {
  auto messageData = std::vector<std::byte>(messageSize);
  const auto bytes = co_await boost::asio::async_read(
      mSocket, boost::asio::buffer(messageData), boost::asio::use_awaitable);
  if (bytes != messageSize) {
    throw std::runtime_error("Invalid message body received");
  }
  const auto message = chat::Message::decode(messageData);
  co_return message;
}

boost::asio::awaitable<void> Session::writer(const ErrorHandler &errorHandler) {
  try {
    while (mSocket.lowest_layer().is_open()) {
      if (!mMessages.empty()) {
        const auto messageData = mMessages.front()->encode();
        const auto headerData = Message::encodeHeader(messageData.size());
        const auto buffers = std::vector<boost::asio::const_buffer>{
            boost::asio::buffer(headerData), boost::asio::buffer(messageData)};
        co_await boost::asio::async_write(mSocket, buffers,
                                          boost::asio::use_awaitable);
        mMessages.pop();
      } else {
        try {
          co_await mTimer.async_wait(boost::asio::use_awaitable);
        } catch (const std::exception &exception) {
          // Ignore. TODO: check explicitly for operation cancelled
        }
      }
    }
  } catch (const std::exception &exception) {
    close();
    errorHandler();
    co_return;
  }
}

void Session::setUserName(std::string userName) {
  mUserName = std::move(userName);
}

void Session::close() {
  boost::system::error_code error;
  mSocket.shutdown(error);
  if (error == boost::asio::error::eof ||
      error == boost::asio::error::operation_aborted) {
    // Rationale:
    // http://stackoverflow.com/questions/25587403/boost-asio-ssl-async-shutdown-always-finishes-with-an-error
    error = {};
  }
  if (error) {
    std::cerr << error.message() << "\n";
  }
  mSocket.lowest_layer().close();
  mTimer.cancel();
}

void Session::post(const std::shared_ptr<const Message> &message) {
  mMessages.push(message);
  mTimer.cancel_one();
}

std::string_view Session::userName() const { return mUserName; }

} // namespace chat
