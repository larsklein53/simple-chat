#include <boost/asio.hpp>
#include <boost/asio/awaitable.hpp>
#include <boost/asio/co_spawn.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/asio/use_awaitable.hpp>
#include <boost/lexical_cast.hpp>
#include <charconv>
#include <deque>
#include <iostream>

#include "Message.h"
#include "Session.h"

namespace chat {

class Client {
public:
  Client(boost::asio::io_context &ioContext,
         boost::asio::ssl::context &sslContext,
         const boost::asio::ip::tcp::resolver::results_type &endpoints)
      : mIOContext{ioContext}, mSSLContext{sslContext},
        mSession{std::make_shared<Session>(mIOContext, mSSLContext)},
        mEndpoints{endpoints}, mReconnectTimer{mIOContext}, mSignals{
                                                                mIOContext} {
    auto &socket = mSession->getSocket();
    socket.set_verify_mode(boost::asio::ssl::verify_peer);
    socket.set_verify_callback(
        [](bool preVerified, boost::asio::ssl::verify_context & /*unused*/) {
          // TODO: Correct verification
          return preVerified;
        });
    mSignals.add(SIGINT);
    mSignals.add(SIGTERM);
    mSignals.async_wait(
        [this](const boost::system::error_code &error, int /*unused*/) {
          if (!error) {
            mIOContext.stop();
            mSession->close();
          }
        });
    boost::asio::co_spawn(mIOContext, tryConnect(), boost::asio::detached);
  }

  void write(const std::shared_ptr<const Message> &msg) { mSession->post(msg); }

  void close() {
    mIOContext.post([this]() {
      mIOContext.stop();
      mSession->close();
    });
  }

private:
  boost::asio::awaitable<void> tryConnect() {
    try {
      co_await boost::asio::async_connect(mSession->getSocket().lowest_layer(),
                                          mEndpoints,
                                          boost::asio::use_awaitable);
      std::cout << "Connected \n";
    } catch (const std::exception &exception) {
      tryReconnect();
      co_return;
    }
    co_await mSession->start(
        [](const std::shared_ptr<const Message> &message) {
          assert(message->type() == Message::MessageType::TEXT);
          std::cout << message->body() << "\n";
        },
        [this]() { tryReconnect(); }, boost::asio::ssl::stream_base::client);
  }

  void tryReconnect() {
    std::cerr << "Disconnected. Trying again in 3 seconds \n";
    co_spawn(
        mIOContext,
        [this]() -> boost::asio::awaitable<void> {
          mSession.reset(new Session{mIOContext, mSSLContext});
          mReconnectTimer.expires_from_now(std::chrono::seconds{3});
          co_await mReconnectTimer.async_wait(boost::asio::use_awaitable);
          co_await tryConnect();
        },
        boost::asio::detached);
  }

private:
  boost::asio::io_context &mIOContext;
  boost::asio::ssl::context &mSSLContext;
  std::shared_ptr<Session> mSession;
  const boost::asio::ip::tcp::resolver::results_type mEndpoints;
  boost::asio::steady_timer mReconnectTimer;
  boost::asio::signal_set mSignals;
};

} // namespace chat

int main(int argc, char *argv[]) {
  constexpr std::string_view loginCmd{"LOGIN: "};
  constexpr std::string_view messageCmd{"MESSAGE: "};
  constexpr std::string_view stopCmd{"STOP:"};
  if (argc != 3) {
    std::cout << "Usage: [Host] [Port] \n";
    return EXIT_SUCCESS;
  }
  try {
    const auto host = std::string_view{argv[1]};
    const auto port = std::string_view{argv[2]};
    boost::asio::io_context ioContext;
    auto resolver = boost::asio::ip::tcp::resolver{ioContext};
    auto endpoints = resolver.resolve(host, port);
    auto sslContext =
        boost::asio::ssl::context{boost::asio::ssl::context::sslv23_client};
    sslContext.load_verify_file("certs/cert.pem");
    auto client = chat::Client{ioContext, sslContext, endpoints};
    auto thread = std::thread([&ioContext]() { ioContext.run(); });
    for (std::string line; std::getline(std::cin, line);) {
      // TODO: Improve
      if (line.starts_with(loginCmd)) {
        const auto body = line.substr(loginCmd.length());
        client.write(std::make_shared<chat::Message>(
            chat::Message::MessageType::LOGIN, body));
      } else if (line.starts_with(messageCmd)) {
        const auto body = line.substr(messageCmd.length());
        client.write(std::make_shared<chat::Message>(
            chat::Message::MessageType::TEXT, body));
      } else if (line.starts_with(stopCmd)) {
        break;
      } else {
        std::cerr << "Invalid request. Use ['LOGIN: '|'MESSAGE: '|'STOP:'] \n";
      }
    }
    client.close();
    thread.join();
    return EXIT_SUCCESS;
  } catch (const std::exception &e) {
    std::cerr << "Error: " << e.what() << "\n";
    return EXIT_FAILURE;
  }
}
