#pragma once

#include <array>
#include <string>
#include <vector>

namespace chat {

// TODO: Split into request/response
class Message {
public:
  static constexpr char MessageHeaderSize = sizeof(size_t);

  enum class MessageType : char { LOGIN = 0, TEXT = 1 };

  Message(MessageType messageType, std::string body);

  MessageType type() const;

  const std::string &body() const;

  // Assume same byte order on server and client
  static std::size_t
  decodeHeader(const std::array<std::byte, MessageHeaderSize> &bytes);
  static std::array<std::byte, MessageHeaderSize>
  encodeHeader(std::size_t value);

  std::vector<std::byte> encode() const;
  static std::shared_ptr<Message>
  decode(const std::vector<std::byte> &messageData);

private:
  MessageType mMessageType;
  std::string mBody;
};
} // namespace chat
