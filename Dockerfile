FROM ubuntu:groovy AS build

ARG CMAKE_ARGS="-DCMAKE_BUILD_TYPE=RELEASE"
ENV LANG="C.UTF-8"

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
    apt-utils \
    dialog \
    gnupg \
    && apt-get update \
    && apt-get install -y --no-install-recommends \
    ca-certificates \
    lsb-release \
    software-properties-common \
    wget \
    cmake \
    g++ \
    ninja-build \
    libssl-dev \
    && rm -rf /var/lib/apt/lists/* \
    && wget https://apt.llvm.org/llvm.sh \
    && chmod +x llvm.sh \
    && ./llvm.sh 12 \
    && apt-get update \
    && apt-get install -y --no-install-recommends \
    libc++-12-dev \
    libc++abi-12-dev \
    && rm -rf /var/lib/apt/lists/* \
    && update-alternatives --install /usr/bin/cc cc /usr/bin/clang-12 100 \
    && update-alternatives --install /usr/bin/c++ c++ /usr/bin/clang++-12 100

ARG BOOST_SHA256=aeb26f80e80945e82ee93e5939baebdca47b9dee80a07d3144be1e1a6a66dd6a
RUN cd /tmp \
    && wget https://dl.bintray.com/boostorg/release/1.75.0/source/boost_1_75_0.tar.gz \
    && echo "${BOOST_SHA256} /tmp/boost_1_75_0.tar.gz" | sha256sum -c - \
    && tar xzf /tmp/boost_1_75_0.tar.gz -C /tmp/ \
    && rm /tmp/boost_1_75_0.tar.gz \
    && cd /tmp/boost_1_75_0 \
    && ./bootstrap.sh --with-libraries=system \
    && ./b2 install

RUN mkdir -p /chat/build/
COPY . /chat
WORKDIR /chat/build/
RUN cmake -GNinja ${CMAKE_ARGS} ../
RUN ninja

FROM build

COPY --from=build /chat/build/server /chat/build/client /bin/chat/
COPY --from=build /chat/build/certs /bin/chat/certs
WORKDIR /bin/chat
ENTRYPOINT [ "/bin/chat/server" ]
