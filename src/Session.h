#pragma once

#include <boost/asio.hpp>
#include <boost/asio/awaitable.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/asio/strand.hpp>

#include "Message.h"

#include <queue>

namespace chat {
class Session : public std::enable_shared_from_this<Session> {
public:
  using MessageHandler =
      std::function<void(const std::shared_ptr<const Message> &)>;
  using ErrorHandler = std::function<void()>;
  using SSLSocket = boost::asio::ssl::stream<boost::asio::ip::tcp::socket>;

  Session(SSLSocket socket);

  Session(boost::asio::ip::tcp::socket socket,
          boost::asio::ssl::context &sslContext);

  Session(boost::asio::io_context &ioContext,
          boost::asio::ssl::context &sslContext);

  SSLSocket &getSocket();

  boost::asio::awaitable<void>
  start(MessageHandler &&messageHandler, ErrorHandler &&errorHandler,
        boost::asio::ssl::stream_base::handshake_type);

  void setUserName(std::string userName);

  void post(const std::shared_ptr<const Message> &message);

  std::string_view userName() const;
  void close();

private:
  void doHandshake();
  boost::asio::awaitable<void> reader(const MessageHandler &messageHandler,
                                      const ErrorHandler &errorHandler);
  boost::asio::awaitable<std::shared_ptr<Message>>
  readBody(std::size_t messageSize);
  boost::asio::awaitable<void> writer(const ErrorHandler &errorHandler);

  SSLSocket mSocket;
  boost::asio::steady_timer mTimer;
  boost::asio::strand<SSLSocket::executor_type> mReadStrand;
  boost::asio::strand<SSLSocket::executor_type> mWriteStrand;
  std::string mUserName;
  std::queue<const std::shared_ptr<const Message>> mMessages;
};
} // namespace chat
