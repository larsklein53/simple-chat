#pragma once

#include <boost/asio.hpp>
#include <boost/asio/awaitable.hpp>
#include <boost/asio/ssl.hpp>

#include "ClientRegistry.h"
#include "Session.h"

namespace chat {

class Server {
public:
  explicit Server(unsigned short port, std::size_t thread_pool_size);

  void run();

private:
  boost::asio::awaitable<void> handleAccept();

  void handleStop();

  std::size_t mThreadPoolSize;
  boost::asio::io_context mIOContext;
  boost::asio::signal_set mSignals;
  boost::asio::ip::tcp::acceptor mAcceptor;
  boost::asio::ssl::context mSSLContext;
  ClientRegistry mClients;


};
} // namespace chat
