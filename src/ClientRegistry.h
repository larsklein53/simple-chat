#pragma once
#include <boost/asio/awaitable.hpp>
#include <iostream>
#include <mutex>
#include <unordered_map>

#include "Session.h"

namespace chat {

class ClientRegistry {
public:
  bool login(const std::string userName, std::shared_ptr<Session> session) {
    std::lock_guard<std::mutex> lock{mMutex};
    session->setUserName(std::move(userName));
    return mMap.try_emplace(session->userName(), std::move(session)).second;
  }

  bool isLoggedIn(const std::shared_ptr<Session> &session) {
    std::lock_guard<std::mutex> lock{mMutex};
    return mMap.contains(session->userName());
  }

  const std::shared_ptr<Session> get(const std::string &userName) {
    std::lock_guard<std::mutex> lock{mMutex};
    const auto it = mMap.find(userName);
    if (it == mMap.end()) {
      return nullptr;
    }
    return it->second;
  }

  void logout(const std::shared_ptr<Session> &session) {
    std::lock_guard<std::mutex> lock{mMutex};
    mMap.erase(session->userName());
  }

  void
  sendMessageToAllClientsExcept(const std::shared_ptr<const Message> &message,
                                const std::shared_ptr<Session> &session) {
    std::lock_guard<std::mutex> lock{mMutex};
    for (const auto &[username, client] : mMap) {
      if (session == client) {
        continue;
      }
      client->post(message);
    }
  }

  void stopAll() {
    std::lock_guard<std::mutex> lock{mMutex};
    for (auto &client : mMap) {
      client.second->close();
    }
    mMap.clear();
  }

private:
  std::unordered_map<std::string_view, std::shared_ptr<Session>> mMap;
  std::mutex mMutex;
};
} // namespace chat
