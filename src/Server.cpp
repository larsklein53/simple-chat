#include <boost/asio/co_spawn.hpp>
#include <boost/asio/use_awaitable.hpp>
#include <boost/lexical_cast.hpp>
#include <iostream>

#include "Server.h"
#include "Session.h"

namespace chat {

namespace {
const auto loginMessage =
    std::make_shared<Message>(Message::MessageType::TEXT, "Login Successful");
const auto notLoggedInMessage = std::make_shared<Message>(
    Message::MessageType::TEXT,
    "You have to login, before you can send messages");
const auto unsupportedMessageTypeMessage = std::make_shared<Message>(
    Message::MessageType::TEXT, "Unsupported message type");
const auto invalidUserMessage = std::make_shared<Message>(
    Message::MessageType::TEXT, "Invalid username or username already taken");
} // namespace

Server::Server(const unsigned short port, const std::size_t threadPoolSize)
    : mThreadPoolSize{threadPoolSize}, mSignals{mIOContext},
      mAcceptor{mIOContext, boost::asio::ip::tcp::endpoint(
                                boost::asio::ip::tcp::v4(), port)},
      mSSLContext{boost::asio::ssl::context::sslv23_server} {
  mSignals.add(SIGINT);
  mSignals.add(SIGTERM);
  mSignals.async_wait(
      [this](const boost::system::error_code &error, int /*unused*/) {
        if (!error) {
          handleStop();
        }
      });
  mSSLContext.set_options(boost::asio::ssl::context::default_workarounds |
                          boost::asio::ssl::context::single_dh_use);

  mSSLContext.use_certificate_chain_file("certs/cert.pem");
  mSSLContext.use_private_key_file("certs/key.pem",
                                   boost::asio::ssl::context::pem);
  mSSLContext.use_tmp_dh_file("certs/dh2048.pem");
  mSSLContext.set_verify_mode(boost::asio::ssl::context::verify_peer);
  mSSLContext.load_verify_file("certs/client/client.pem");
  co_spawn(mIOContext, handleAccept(), boost::asio::detached);
}

void Server::run() {
  std::cout << "Server listening on port: " << mAcceptor.local_endpoint().port()
            << std::endl;
  std::vector<std::shared_ptr<std::thread>> threads;
  for (std::size_t i = 0; i < mThreadPoolSize; ++i) {
    auto thread = std::make_shared<std::thread>([this]() { mIOContext.run(); });
    threads.push_back(thread);
  }

  for (auto &thread : threads) {
    thread->join();
  }
}

boost::asio::awaitable<void> Server::handleAccept() {
  while (mAcceptor.is_open()) {
    auto session = std::make_shared<Session>(
        co_await mAcceptor.async_accept(boost::asio::use_awaitable),
        mSSLContext);
    co_await session->start(
        [this, session](const std::shared_ptr<const Message> &message) {
          if (message->type() == Message::MessageType::LOGIN) {
            const auto &userName = message->body();
            if (!mClients.login(userName, session)) {
              session->post(invalidUserMessage);
              return;
            }
            session->post(loginMessage);
            const auto helloMessage = std::make_shared<Message>(
                Message::MessageType::TEXT, userName + " says hello");
            mClients.sendMessageToAllClientsExcept(helloMessage, session);
          } else if (message->type() == Message::MessageType::TEXT) {
            if (!mClients.isLoggedIn(session)) {
              session->post(notLoggedInMessage);
              return;
            }
            mClients.sendMessageToAllClientsExcept(message, session);
          } else {
            session->post(unsupportedMessageTypeMessage);
          }
        },
        [this, session]() {
          const auto logoutMessage = std::make_shared<Message>(
              Message::MessageType::TEXT,
              std::string{session->userName()} + " says goodbye");
          mClients.sendMessageToAllClientsExcept(logoutMessage, session);
          mClients.logout(session);
        },
        boost::asio::ssl::stream_base::server);
  }
}

void Server::handleStop() {
  std::cout << "Shutting down \n";
  mAcceptor.close();
  mClients.stopAll();
  mIOContext.stop();
}
} // namespace chat

int main(int argc, char *argv[]) {
  if (argc != 3) {
    std::cout << "Usage: [Port] [# Threads] \n";
    return EXIT_SUCCESS;
  }

  try {
    const auto port = boost::lexical_cast<unsigned short>(argv[1]);
    const auto threadPoolSize = boost::lexical_cast<std::size_t>(argv[2]);
    auto server = chat::Server{port, threadPoolSize};
    server.run();
    return EXIT_SUCCESS;
  } catch (const std::exception &e) {
    std::cerr << "Error: " << e.what() << "\n";
    return EXIT_FAILURE;
  }
}
